import flask

app = flask.Flask(__name__)


@app.route('/')
def hello():
    return 'Hallo'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=40000)
